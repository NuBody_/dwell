import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
X =0
y=1
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()


def importdata():
    dataset = pd.read_csv('dell2.csv')
    
    X = dataset.iloc[:, [4,6,7]].values
    y = dataset.iloc[:, 5].values
    return X,y

def train(X,y):
    
    X = sc.fit_transform(X)
    classifier = LogisticRegression(random_state = 0)
    classifier.fit(X, y)
    return classifier
    
    
def test(classifier,ques):
    l=list(ques[0])
    x=np.array([l[6],l[8],l[9]])    
    x=x.reshape(1,-1)
    y_pred=classifier.predict(sc.transform(x))
    
    return str(y_pred[0])    
   

