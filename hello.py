from flask import Flask,render_template
app = Flask(__name__)

@app.route('/')
def hello_world():
   return render_template('index.html')

@app.route('/login')
def login():
	return render_template('login.html')

@app.route('/register')
def register():
	return render_template('register.html')

@app.route('/product')
def product():
   return render_template('product.html') 

@app.route('/shop')
def shop():
   return render_template('shop.html')

@app.route('/cart')
def cart():
   return render_template('cart.html')

@app.route('/cart1')
def cart1():
   return render_template('cart1.html')   

@app.route('/contact')
def contact():
   return render_template('contact.html')

@app.route('/philips')
def philips():
   return render_template('philips.html')

@app.route('/huawei')
def huawei():
   return render_template('huawei.html')   

@app.route('/apple')
def apple():
   return render_template('apple.html')   

@app.route('/sony')
def sony():
   return render_template('sony.html')     

@app.route('/canon')
def canon():
   return render_template('canon.html')  

@app.route('/samsung')
def samsung():
   return render_template('samsung.html')    
   
@app.route('/lenovo')
def lenovo():
   return render_template('lenovo.html')   

@app.route('/digitus')
def digitus():
   return render_template('digitus.html')             

@app.route('/astro')
def astro():
   return render_template('astro.html') 

@app.route('/transcend')
def transcend():
   return render_template('transcend.html')     

@app.route('/xiaomi')
def xiaomi():
   return render_template('xiaomi.html')    

@app.route('/rapoo')
def rapoo():
   return render_template('rapoo.html')         

           


if __name__ == '__main__':
   app.run(debug=True)