import random
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from flask import Flask, render_template, request,redirect
from flask_mysqldb import MySQL
import logistic_regression as lr

app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'root'
app.config['MYSQL_DB'] = 'dwell'

mysql = MySQL(app)


def ml(result):
	X,y=lr.importdata()
	classifier=lr.train(X,y)
	t=lr.test(classifier,result)

	return t

@app.route('/register', methods=['GET', 'POST'])
def index():    
    if request.method == "POST":

    	random_number = random.randint(0, 60)
    	rand = random.randint(1000,90000)

    	details = request.form
    	name = details['name']
    	email = details['email']
    	pswrd = details['pswrd']
    	cur = mysql.connection.cursor()
    	cur.execute("INSERT INTO customer(CustomerName, CustomerEmail,password,addtocart,transaction,ratio,avgPrice,time) VALUES (%s, %s,%s,1,0,0,%s,%s)", (name, email,pswrd,rand,random_number))
    	mysql.connection.commit()
    	cur.close()
    	return redirect('/')
    return render_template('register.html')

@app.route('/shop')
def shop():
   return render_template('shop.html')

@app.route('/shop1')
def shop1():
   return render_template('shop1.html')

@app.route('/cart')
def cart():
  cur = mysql.connection.cursor()
  cur.execute("SELECT * from product");
  data = cur.fetchall()
  return render_template('cart.html',value=data)

@app.route('/cart1')
def cart1():
  cur = mysql.connection.cursor()
  cur.execute("SELECT * from product1");
  data = cur.fetchall()
  return render_template('cart1.html',value=data)

@app.route('/transaction')
def transaction():
   return render_template('transaction.html')   

@app.route('/product1', methods=['GET','POST'])
def product1():
  if request.method == "POST":
    details = request.form
    desc1 = details['mac1']
    url1 = details['url1']
    price1 = details['price1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (desc1, price1,url1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('product1.html') 

@app.route('/product', methods=['GET','POST'])
def product():
  if request.method == "POST":
    details = request.form
    desc = details['mac']
    url = details['url']
    price = details['price']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (desc, price,url))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('product.html')

@app.route('/philips', methods=['GET','POST'])
def philips():
  if request.method == "POST":
    details = request.form
    pdesc = details['phil']
    purl = details['purl']
    pprice = details['pprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (pdesc, pprice,purl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('philips.html')

@app.route('/philips1', methods=['GET','POST'])
def philips1():
  if request.method == "POST":
    details = request.form
    pdesc1 = details['phil1']
    purl1 = details['purl1']
    pprice1 = details['pprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (pdesc1, pprice1,purl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('philips1.html')

@app.route('/huawei', methods=['GET','POST'])
def huawei():
  if request.method == "POST":
    details = request.form
    sdesc = details['speak']
    surl = details['surl']
    sprice = details['sprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (sdesc, sprice,surl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('huawei.html')   

@app.route('/huawei1', methods=['GET','POST'])
def huawei1():
  if request.method == "POST":
    details = request.form
    sdesc1 = details['speak1']
    surl1 = details['surl1']
    sprice1 = details['sprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (sdesc1, sprice1,surl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('huawei1.html')  

@app.route('/apple', methods=['GET','POST'])
def apple():
  if request.method == "POST":
    details = request.form
    adesc = details['apple']
    aurl = details['aurl']
    aprice = details['aprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (adesc, aprice,aurl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('apple.html')   

@app.route('/apple1', methods=['GET','POST'])
def apple1():
  if request.method == "POST":
    details = request.form
    adesc1 = details['apple1']
    aurl1 = details['aurl1']
    aprice1 = details['aprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (adesc1, aprice1,aurl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('apple1.html') 

@app.route('/sony', methods=['GET','POST'])
def sony():
  if request.method == "POST":
    details = request.form
    ndesc = details['sony']
    nurl = details['nurl']
    nprice = details['nprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (ndesc, nprice,nurl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('sony.html')     

@app.route('/sony1', methods=['GET','POST'])
def sony1():
  if request.method == "POST":
    details = request.form
    ndesc1 = details['sony1']
    nurl1 = details['nurl1']
    nprice1 = details['nprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (ndesc1, nprice1,nurl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('sony1.html') 

@app.route('/canon', methods=['GET','POST'])
def canon():
  if request.method == "POST":
    details = request.form
    cdesc = details['can']
    curl = details['curl']
    cprice = details['cprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (cdesc, cprice,curl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('canon.html')  

@app.route('/canon1', methods=['GET','POST'])
def canon1():
  if request.method == "POST":
    details = request.form
    cdesc1 = details['can1']
    curl1 = details['curl1']
    cprice1 = details['cprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (cdesc1, cprice1,curl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('canon1.html')  

@app.route('/samsung', methods=['GET','POST'])
def samsung():
  if request.method == "POST":
    details = request.form
    mdesc = details['sam']
    murl = details['murl']
    mprice = details['mprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (mdesc, mprice,murl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('samsung.html')    

@app.route('/samsung1', methods=['GET','POST'])
def samsung1():
  if request.method == "POST":
    details = request.form
    mdesc1 = details['sam1']
    murl1 = details['murl1']
    mprice1 = details['mprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (mdesc1, mprice1,murl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('samsung1.html')    
   
@app.route('/lenovo', methods=['GET','POST'])
def lenovo():
  if request.method == "POST":
    details = request.form
    ldesc = details['len']
    lurl = details['lurl']
    lprice = details['lprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (ldesc, lprice,lurl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('lenovo.html')   

@app.route('/lenovo1', methods=['GET','POST'])
def lenovo1():
  if request.method == "POST":
    details = request.form
    ldesc1 = details['len1']
    lurl1 = details['lurl1']
    lprice1 = details['lprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (ldesc1, lprice1,lurl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('lenovo1.html')   

@app.route('/digitus', methods=['GET','POST'])
def digitus():
  if request.method == "POST":
    details = request.form
    ddesc = details['dig']
    durl = details['durl']
    dprice = details['dprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (ddesc, dprice,durl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('digitus.html')     

@app.route('/digitus1', methods=['GET','POST'])
def digitus1():
  if request.method == "POST":
    details = request.form
    ddesc1 = details['dig1']
    durl1 = details['durl1']
    dprice1 = details['dprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (ddesc1, dprice1,durl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('digitus1.html')            

@app.route('/astro', methods=['GET','POST'])
def astro():
  if request.method == "POST":
    details = request.form
    adesc = details['ass']
    aurl = details['aurl']
    aprice = details['aprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (adesc, aprice,aurl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('astro.html') 

@app.route('/astro1', methods=['GET','POST'])
def astro1():
  if request.method == "POST":
    details = request.form
    adesc1 = details['ass1']
    aurl1 = details['aurl1']
    aprice1 = details['aprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (adesc1, aprice1,aurl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('astro1.html') 

@app.route('/transcend', methods=['GET','POST'])
def transcend():
  if request.method == "POST":
    details = request.form
    tdesc = details['tran']
    turl = details['turl']
    tprice = details['tprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (tdesc, tprice,turl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('transcend.html')     

@app.route('/transcend1', methods=['GET','POST'])
def transcend1():
  if request.method == "POST":
    details = request.form
    tdesc1 = details['tran1']
    turl1 = details['turl1']
    tprice1 = details['tprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (tdesc1, tprice1,turl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('transcend1.html')     

@app.route('/xiaomi', methods=['GET','POST'])
def xiaomi():
  if request.method == "POST":
    details = request.form
    mdesc = details['mi']
    murl = details['murl']
    mprice = details['mprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (mdesc, mprice,murl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('xiaomi.html')    

@app.route('/xiaomi1', methods=['GET','POST'])
def xiaomi1():
  if request.method == "POST":
    details = request.form
    mdesc1 = details['mi1']
    murl1 = details['murl1']
    mprice1 = details['mprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (mdesc1, mprice1,murl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('xiaomi1.html')  

@app.route('/rapoo', methods=['GET', 'POST'])
def rapoo():
  if request.method == "POST":
    details = request.form
    rdesc = details['rap']
    rurl = details['rurl']
    rprice = details['rprice']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product(name,quantity,price,url) VALUES (%s,1, %s,%s)", (rdesc, rprice,rurl))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart')
  return render_template('rapoo.html')

@app.route('/rapoo1', methods=['GET', 'POST'])
def rapoo1():
  if request.method == "POST":
    details = request.form
    rdesc1 = details['rap1']
    rurl1 = details['rurl1']
    rprice1 = details['rprice1']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO product1(name,quantity,price,url) VALUES (%s,1, %s,%s)", (rdesc1, rprice1,rurl1))
    mysql.connection.commit()
    cur.close()
    return redirect('/cart1')
  return render_template('rapoo1.html')            

@app.route('/index')
def home():
   return render_template('index.html')

@app.route('/index1')
def home1():
   return render_template('index1.html')

@app.route('/login')
def logout():
   return redirect('/')

@app.route('/contact')
def contact():
   return render_template('contact.html')

@app.route('/contact1')
def contact1():
   return render_template('contact.html')   

@app.route('/', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
    	details = request.form
    	e_mail = details['e-mail']
    	passwrd = details['password']
    	cur = mysql.connection.cursor()
    	cur.execute("SELECT * from customer WHERE CustomerEmail =%s and password =%s",(e_mail,passwrd))
    	result = cur.fetchall()
    	if cur.rowcount >0:
    		flag= ml(result)
    		if (flag=='0'):
    			return render_template('index.html')
    		elif (flag=='1'):
    			return render_template('index1.html')
    	else:
    		return 'fail'
    return render_template('login.html', error=error)


if __name__ == '__main__':
    app.run(debug=True)


